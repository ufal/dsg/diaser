from copy import deepcopy
from data_processing.da import DAI
#[{
#  “dial_id”: 123,
#  “domains”: [“restaurant”, “train”],
#  “utterances”: [
#    {
#      “speaker”: “USER”,
#      “turn”: 0, # same for USER and SYSTEM in one turn
#      “utterance”: “I need to find a restaurant that serves chinese.”,
#      “nlu”: {
#        “domain”: “restaurant”,
#        “slots”: {“food”: “chinese”},
#        “intent”: “inform_restaurant”
#      } # end nlu
#      “state”: {
#        “restaurant”: {“food”: “chinese”}
#      } # end state
#      “db”: ?,
#      “inconsistency”: ?,
#    } # end turn
#   ...
#       ] # end turn_list
#} # end dialogue
#...
#] # end data


NOT_SET = None


class Dialogue:
    def __init__(self,
                 dialogue_id=NOT_SET,
                 domains=NOT_SET,
                 utterances=NOT_SET,
                 success=NOT_SET):
        self._data = dict()
        self._data['dialogue_id'] = dialogue_id
        self._data['domains'] = domains
        self._data['utterances'] = [Utterance.load(u) for u in utterances]

        self._data['success'] = success

    def __getitem__(self, item):
        if item in self._data:
            return self._data[item]
        return None

    def dump(self):
        data = deepcopy(self._data)
        data['utterances'] = [u.dump() for u in self._data['utterances']]
        return data

    @property
    def system_utterances(self):
        return [u for u in self._data['utterances'] if u.is_system]

    @property
    def user_utterances(self):
        return [u for u in self._data['utterances'] if u.is_user]

    @property
    def merged_turns(self):
        utterances = self._data['utterances']
        for i in range(0, len(utterances), 2):
            yield utterances[i], utterances[i+1]

    @staticmethod
    def load(dct):
        return Dialogue(dialogue_id=dct['dialogue_id'],
                        domains=dct['domains'],
                        utterances=dct['utterances'])

    def extend_user_acts(self, acts):
        for tid, act in acts.items():
            self._data['utterances'][int(tid)].extend_nlu(act)


class Utterance:
    def __init__(self,
                 actor=NOT_SET,
                 turn=NOT_SET,
                 utterance=NOT_SET,
                 delex_utterance=NOT_SET,
                 nlu=NOT_SET,
                 intent=NOT_SET,
                 state=NOT_SET,
                 inconsistency=NOT_SET,
                 database=NOT_SET
                 ):
        self._data = dict()
        self._data['actor'] = actor.lower()
        self._data['turn'] = turn
        self._data['utterance'] = utterance
        self._data['delex_utterance'] = delex_utterance
        self._data['intent'] = intent
        self._data['inconsistency'] = inconsistency
        self._data['database'] = database
        if isinstance(nlu, list) and \
                len(nlu) > 0 and isinstance(nlu[0], str):
            self._data['nlu'] = [DAI.parse(dai) for dai in nlu]
        else:
            self._data['nlu'] = nlu
        self._data['state'] = state

    def dump(self):
        data = deepcopy(self._data)
        data['nlu'] = [str(dai) for dai in data['nlu']]
        return data

    @property
    def is_system(self):
        return self._data['actor'] == 'system'

    @property
    def is_user(self):
        return not self.is_system

    def __getitem__(self, item):
        if item in self._data:
            return self._data[item]
        return None

    def extend_nlu(self, act):

        def _tr(key):
            key = key.replace('leaveat', 'leave at')\
                    .replace('arriveby', 'arrive by')\
                    .replace('bookstay', 'book stay')\
                    .replace('bookday', 'book day')\
                    .replace('bookpeople', 'book people')\
                    .replace('entrancefee', 'entrance fee')\
                    .replace('trainid', 'train id')
            return key

        substitutions = []
        for intent, slots in act['dialog_act'].items():
            for slot, val in slots:
                dai = DAI(intent, slot, val)
                self._data['nlu'].append(dai)

        for slot in act['span_info']:
            start = slot[3]
            end = slot[4]
            if start != end:
                key = slot[1]
                substitutions.append((start, end, _tr(key)))
        delex_text = self._data['utterance']
        for s, e, k in substitutions:
            delex_text = f'{delex_text[:s]}[{k}]{delex_text[e:]}'
        self._data['delex_utterance'] = delex_text


    @staticmethod
    def load(dct):
        if isinstance(dct, Utterance):
            return dct
        return Utterance(actor=dct['actor'],
                         turn=dct['turn'],
                         utterance=dct['utterance'],
                         nlu=dct['nlu'],
                         intent=dct['intent'],
                         state=dct['state'])
