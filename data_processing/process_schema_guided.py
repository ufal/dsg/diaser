import argparse
import glob
import os
import json

from data_processing.annotation_schema import Dialogue, Utterance
from data_processing.merge_data import write_json
from data_processing.da import DAI
from data_processing.ontology_unifier import get_ontology_unifier


def load_split(data_dir, ontology_unifier):
    data = []
    for fn in glob.glob(f'{data_dir}/*json'):
        data.extend(load_file(fn, ontology_unifier))
    return data


def load_file(fn, ontology_unifier):
    with open(fn, 'rt') as fd:
        data = json.load(fd)
    loaded = []
    for dial in data:
        if 'turns' not in dial:
            continue
        utterances = []
        for n, turn in enumerate(dial['turns']):
            state_dict = dict()
            nlu = dict()
            substitutions = []
            for frame in turn['frames']:
                if 'slots' in frame:
                    for slot in frame['slots']:
                        if "start" not in slot:
                            continue
                        start = slot["start"]
                        end = slot["exclusive_end"]
                        if start != end:
                            key = ontology_unifier.map_slot(slot["slot"])
                            substitutions.append((start, end, key))

                nlu.update({act['slot']: (act['act'], ' '.join(act['values'])) for act in frame['actions']})
                if 'state' in frame:
                    state = frame['state']
                    intent = frame['state']['active_intent']
                    for sl, val in state['slot_values'].items():
                        val = ' '.join(val)
                        state_dict[sl] = val

            substitutions.sort(key=lambda x: x[0], reverse=True)
            delex_text = turn['utterance']
            for s, e, k in substitutions:
                delex_text = f'{delex_text[:s]}[{k}]{delex_text[e:]}'

            utt = Utterance(
                actor=turn['speaker'],
                turn=n // 2 + 1,
                utterance=turn['utterance'],
                delex_utterance=delex_text,
                nlu=[DAI(val[0], ontology_unifier.map_slot(slot), val[1]) for slot, val in nlu.items()],
                intent=intent,
                state=state_dict
            )
            utterances.append(utt)
        dial_obj = Dialogue(
            dialogue_id=dial['dialogue_id'],
            domains=[ontology_unifier.map_domain(service) for service in dial['services']],
            utterances=utterances,
        )
        loaded.append(dial_obj.dump())
    return loaded


def main(args):
    ontology_unifier = get_ontology_unifier(args.dataset)
    data = load_split(os.path.join(args.data_dir, args.split), ontology_unifier)
    write_json(args.output_file, data, compress=args.compress)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir')
    parser.add_argument('--dataset', choices=['multiwoz', 'schema'])
    parser.add_argument('--split', default='train')
    parser.add_argument('--output_file')
    parser.add_argument('--compress', action='store_true')
    args = parser.parse_args()
    main(args)
