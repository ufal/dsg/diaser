def get_ontology_unifier(dataset_name, domains=None):
    return {'schema' : SchemaGuidedOntologyUnifier(domains),
            'multiwoz': MultiWOZOntologyUnifier(domains)
           }[dataset_name]


class OntologyUnifier():

    def __init__(self, domains, remove_underscores=True):
        super(OntologyUnifier, self).__init__()
        self._domains = domains
        self._domain_mapping = {}
        self._slot_mapping = {}
        self._remove_underscores = remove_underscores
        
    def _init_mappings(self):

        # substitute "_" with " " in domain and slot names
        if self._remove_underscores:

            new_domain_mapping = {}
            for k in self._domain_mapping.keys():
                new_domain_mapping[k.replace("_", " ")] = self._domain_mapping[k]
            self._domain_mapping = new_domain_mapping

            new_slot_mapping = {}
            for k, v in self._slot_mapping.items():
                new_slot_mapping[k] = v.replace("_", " ")
            self._slot_mapping = new_slot_mapping
                
        if self._domains is not None:
            # filter out unwanted target domains
            new_mapping = {}
            for k in self._domains:
                if k in self._domain_mapping.keys():
                    new_mapping[k] = self._domain_mapping[k]
            self._domain_mapping = new_mapping
        else:
            # get list of actual target domains
            self._domains = list(self._domain_mapping.keys())

        # prepare mappings
        self._original_domains = sum(self._domain_mapping.values(), [])
        self._original_new_domain_mapping = {x: key for key, value in self._domain_mapping.items() for x in value }

    def get_domains(self):
        return self._domains

    def get_original_domains(self):
        return self._original_domains

    def map_domain(self, original_domain):
        if original_domain in self._original_new_domain_mapping:
            return self._original_new_domain_mapping[original_domain]
        else:
            return original_domain

    def map_domain_reverse(self, mapped_domain):
        return self._domain_mapping[mapped_domain]

    def map_slot(self, original_slot):
        if original_slot in self._slot_mapping:
            return self._slot_mapping[original_slot]
        else:
            if self._remove_underscores:
                slot = original_slot.replace("_", " ")
            self._slot_mapping[original_slot] = slot
            return slot


class MultiWOZOntologyUnifier(OntologyUnifier):

    def __init__(self, domains):
        super(MultiWOZOntologyUnifier, self).__init__(domains)
        self._init_mappings()


class SchemaGuidedOntologyUnifier(OntologyUnifier):

    def __init__(self, domains):
        super(SchemaGuidedOntologyUnifier, self).__init__(domains)

        # see schema_guided_dataloader.py for a complete list of domains
        self._domain_mapping = {
            'hotel' :       ['Hotels_1', 'Hotels_2', 'Hotels_3', 'Hotels_4'], 
            'train' :       ['Trains_1'],
            'attraction' :  ['Travel_1'],
            'restaurant' :  ['Restaurants_1', 'Restaurants_2'], 
            'taxi' :        ['RideSharing_1', 'RideSharing_2'],
            'bus' :         ['Buses_1', 'Buses_2', 'Buses_3'],
            'flight' :      ['Flights_1', 'Flights_2', 'Flights_3', 'Flights_4'],
            'music' :       ['Music_1', 'Music_2', 'Music_3'], 
            'movie' :       ['Media_1', 'Media_2', 'Media_3', 'Movies_1', 'Movies_2', 'Movies_3'], 
            'service' :     ['Services_1', 'Services_2', 'Services_3', 'Services_4'], 
            'bank' :        ['Banks_1', 'Banks_2', 'Payment_1'], 
            'event' :       ['Events_1', 'Events_2', 'Events_3'],  
            'rentalcar' :   ['RentalCars_1', 'RentalCars_2', 'RentalCars_3'],
            'apartment' :   ['Homes_1', 'Homes_2'], 
            'calendar' :    ['Calendar_1'], 
            'weather' :     ['Weather_1'],
            'alarm' :       ['Alarm_1'],
            'messaging' :   ['Messaging_1']
        }

        self._slot_mapping = {
                'star_rating' :    'stars',
                'hotel_name' :      'name',
                'number_of_days':   'stay',
                'has_wifi' :        'internet',
                'check_in_date' :   'check in', # date ?
                'street_address': 'address',
                'phone_number': 'phone',
                'price_per_night': 'price_range',
                'stay_length': 'stay',
                'where_to' :   'destination',
                'check_out_date' :   'check_out',
                'place_name' :      'name',
                'journey_start_time' :  'leave_at',
                'to' :                  'destination',
                'from':                 'departure',
                'category' : 'type',
                'attraction_name' : 'name',
                'number_of_riders' : 'people',
                'from_location' :  'departure',
                'to_location' :    'destination',
                'leaving_time':    'leave_at',
                'leaving_date' :   'date',
                'travelers':       'people',
                'origin' :          'departure',
                'departure_time':   'leave_at',
                'group_size':       'people',
                'departure_date' :   'date',
                'from_city' :  'departure',
                'to_city' :    'destination',
                'price_range' :     'price_range',
                'cuisine':          'food',
                'restaurant_name' : 'name',
                'track' :     'song_name',
                'cast' :            'starring',
                'title' :       'name',
                'directed_by' :     'director',
                'device' :    'playback_device',
                'outbound_departure_time': 'leave at',
                'inbound_departure_time': 'leave at',
                'number_checked_bags' : 'people',
                'actors' : 'starring',
                'recipient_account_name' : 'recipient',
                'transfer_amount' : 'amount',
                'receiver' : 'receiver',
                'city_of_event' : 'city',
                'event_name' : 'name',
                'number_of_seats' : 'people',
                'event_type' : 'type',
                'car_type' : 'car',
                'pickup_date': 'date',
                'pickup_location': 'departure',
                'total_price': 'fee',
                'dropoff_time': 'time',
                'dropoff_location': 'destination',
                'dropoff_date': 'date',
                'start_date' :  'date',
                'end_date' :    'date',
                'pickup_time': 'time',
                'car_name': 'car',
        }
        self._init_mappings()

