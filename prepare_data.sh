MULTIWOZ_ROOT=${MULTIWOZ_ROOT:="multiwoz/"}
SCHEMA_ROOT=${SCHEMA_ROOT:="schema/"}
DSTC_ROOT=todo
OUT_DIR=data/
TMP_DIR=/tmp/data.$$

set -x
shopt -s expand_aliases
alias python=python3
mkdir -p ${TMP_DIR}/{train,dev,test} ${OUT_DIR}

# converts the data into our annotation structure
for split in train dev test; do
	python -m data_processing.process_schema_guided --data_dir ${MULTIWOZ_ROOT} --compress --split ${split} --output_file ${TMP_DIR}/${split}/${split}_mw.json --dataset multiwoz
	python -m data_processing.process_schema_guided --data_dir ${SCHEMA_ROOT} --compress --split ${split} --output_file ${TMP_DIR}/${split}/${split}_schema.json --dataset schema
done

# adds nlu annotation from dialogue acts into multiwoz
for split in train dev test; do
	python -m data_processing.extend_mw_annotation --input_file ${TMP_DIR}/${split}/${split}_mw.json.zip --das_file ${MULTIWOZ_ROOT}/dialog_acts.json --output_file ${TMP_DIR}/${split}/${split}_mw_acts.json --compress
	rm ${TMP_DIR}/${split}/${split}_mw.json.zip
done

# merges the two files into one
for split in train dev test; do
	python -m data_processing.merge_data --data_dir ${TMP_DIR}/${split} --output_file  ${OUT_DIR}/${split}.json --compress
done
# rm -rf ${TMP_DIR}
